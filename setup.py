#!/usr/bin/env python3
import sys
from distutils.core import setup
from distutils.command.install import install as _install

def _post_install(dir):
    from subprocess import call
    call([sys.executable, 'build-aux/post_install.py'])

class install(_install):
    def run(self):
        _install.run(self)
        self.execute(_post_install, (self.install_lib,),
                     msg="- Running post-installation script")

install_data = [
    ('share/glib-2.0/schemas', ["data/com.gitlab.cliodynamicpragmatics.launchy.gschema.xml"]),
    ("share/icons/hicolor/128x128/apps", ["data/icons/hicolor/128/com.gitlab.cliodynamicpragmatics.launchy.png"]),
    ("share/icons/hicolor/48x48/apps", ["data/icons/hicolor/48/com.gitlab.cliodynamicpragmatics.launchy.png"]),
    ("share/applications", ["data/com.gitlab.cliodynamicpragmatics.launchy.desktop"]),
    ("share/launchy/", ["data/chrome/userChrome.css"]),
    ("share/launchy/", ["data/user.js"]),
    ("bin/launchy", ["src/main.py"]),
    ("bin/launchy", ["src/editor.py"]),
    ("bin/launchy", ["src/window.py"]),
    ("bin/launchy", ["src/welcome.py"]),
    ("bin/launchy", ["src/webapps.py"]),
    ("bin/launchy", ["src/constants.py"]),
    ("bin/launchy", ["src/desktopfile.py"]),
    ("bin/launchy", ["src/browseractions.py"]),
    ("bin/launchy", ["src/launchyappbutton.py"]),
    ("bin/launchy", ["src/desktopfilemanager.py"]),
    ("bin/launchy", ["src/__init__.py"])
]

setup(
    name='Launchy',
    version='1.0',
    author='CliodynamicPragmatics',
    description='A Launcher for linux',
    # TODO
    # url=''
    license='GNU GPL3',
    scripts=['com.gitlab.cliodynamicpragmatics.launchy'],
    # TODO
    # packages=[''],
    data_files=install_data,
    cmdclass={'install': install}
)
