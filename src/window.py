#!/usr/bin/env python3

import gi
import sys
import os

gi.require_version('Gtk', '3.0')
gi.require_version('Granite', '1.0')
gi.require_version('Handy', '1')

from gi.repository import Gtk, Gio, Gdk, Handy, Granite

import welcome as wl
import editor as ed
import constants
import webapps as wapps

class LaunchyWindow(Handy.Window):
    Handy.init()

    def __init__(self, app_obj):
        super().__init__(
            title='Launchy',
            application=app_obj,
            # TODO: Ajustar según resolución
            default_height=768/3,
            default_width=1366/3,
            window_position=Gtk.WindowPosition.MOUSE,
            resizable=False
            # TODO: Esto puede afectar el uso...
            #modal=True
        )

        rx, ry = self.get_position()
        self.move(rx, ry)

        self.gio_settings = Gio.Settings(
            schema_id=constants.App.application_id
        )

        self.hdy_deck=self.new_deck()

        self.welcome_view = self.new_welcome_view()
        self.hdy_deck.add(widget=self.welcome_view)

        self.wapps_view = self.new_webapps_view()
        self.hdy_deck.add(widget=self.wapps_view)

        self.edit_view = self.new_edit_view()
        self.hdy_deck.add(widget=self.edit_view)

        if len(os.listdir(constants.Syspaths.apps_folder)) == 0:
            self.hdy_deck.set_visible_child(self.welcome_view)
        else:
            self.hdy_deck.set_visible_child(self.wapps_view)
            self.hdy_deck.remove(widget=self.welcome_view)

        self.add(widget=self.hdy_deck)
        self.show_all()

    def new_deck(self):
        deck = Handy.Deck.new()
        deck.set_transition_duration(duration=500)
        deck.set_can_swipe_back(can_swipe_back=False)
        deck.set_can_swipe_forward(can_swipe_forward=False)
        return deck

    def go_to_edit_view(self):
        self.hdy_deck.set_visible_child(self.edit_view)

    def remove_welcome_view(self):
        self.hdy_deck.remove(widget=self.welcome_view)
        web_apps_view = self.new_webapps_view()
        self.hdy_deck.add(widget=web_apps_view)
        self.show_all()

    def new_welcome_view(self):
        view = wl.WelcomeView()
        return view

    def new_webapps_view(self):
        view = wapps.WebAppView()
        return view

    def new_edit_view(self):
        view = ed.EditView()
        return view
