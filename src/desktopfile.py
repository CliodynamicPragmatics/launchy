#!/usr/bin/env python3

import gi
import constants
import warnings

import browseractions as browse

from gi.repository import GLib

class LaunchyDesktopFile:
    def create(appname, appurl, appicon, appcategory):
        the_file = GLib.KeyFile.new()
        the_file.load_from_data(
            constants.Templates.desktopapp,
            len(constants.Templates.desktopapp),
            GLib.KeyFileFlags(0)
        )

        profile_name = appname.replace(" ", "")
        cmdstr = browse.BrowserActions.open_profile(profile_name)

        the_file.set_string("Desktop Entry", "Name", appname)
        the_file.set_string("Desktop Entry", "Exec", cmdstr + appurl)
        the_file.set_string("Desktop Entry", "Icon", appicon)
        the_file.set_string("Desktop Entry", "Categories", appcategory + ";")
        the_file.set_string("Desktop Entry", "StartupWMClass", profile_name)

        the_file.save_to_file(constants.Syspaths.apps_folder + appname + ".desktop")
