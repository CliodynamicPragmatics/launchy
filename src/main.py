#!/usr/bin/env python3

import gi
import sys
import os

gi.require_version('Gtk', '3.0')
gi.require_version('Granite', '1.0')
gi.require_version('Handy', '1')

from gi.repository import Gtk, Gio, Gdk, Handy, Granite

import window
import constants

class Application(Gtk.Application):

    def __init__(self):
        super().__init__(
            application_id=constants.App.application_id,
            # TODO: Gio.ApplicationFlags tiene un flag relevante
            # (IS_LAUNCHER)
            flags=Gio.ApplicationFlags.FLAGS_NONE
        )

        if not os.path.exists(constants.Syspaths.apps_folder):
            os.makedirs(constants.Syspaths.apps_folder)

        if not os.path.exists(constants.Syspaths.launchy_folder):
            os.makedirs(constants.Syspaths.launchy_folder)

        # TODO: ¿Es realmente necesario tener esto aquí?
        # self.granite_settings = Granite.Settings()
        # self.gtk_settings = Gtk.Settings.get_default()

    def do_startup(self):
        Gtk.Application.do_startup(self)

    # TODO: Hacer esto pytonico
    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = window.LaunchyWindow(self)
        win.present()

    def do_shutdown(self):
        Gtk.Application.do_shutdown(self)

app = Application()
app.run(sys.argv)
