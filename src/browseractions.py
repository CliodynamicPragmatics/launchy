#!/usr/bin/env python3

import sys
import os
from pathlib import Path

class BrowserActions:

    def create_profile_argument(profile_name):
        flt_exec = "/usr/bin/flatpak run "
        flt_cmd = "io.gitlab.librewolf-community "
        flt_args = "-no-remote -CreateProfile "
        string = flt_exec + flt_cmd + flt_args + profile_name
        return string

    def open_profile(profile):
        flt_exec = "/usr/bin/flatpak run "
        flt_cmd = "io.gitlab.librewolf-community "
        flt_args = "-no-remote -P " + profile + " --class=" + profile + " "
        string = flt_exec + flt_cmd + flt_args
        return string

    def get_profile_folder(profile, directory):
        dirlist = [Path(directory + "/" + d) for d in os.listdir(directory)
                   if d.__contains__(profile)]
        return sorted(dirlist, key=lambda x: os.path.getctime(x), reverse=True)[0]
