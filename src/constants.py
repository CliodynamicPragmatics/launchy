#!/usr/bin/env python3

import gi
import os
import locale
import gettext

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk

class App:
    '''Here we are defining our Application infos, so we can easily
    use in all our application files'''
    application_shortname = "launchyapp"
    application_id = "com.gitlab.cliodynamicpragmatics.launchy"
    application_name = "Launchy"
    # application_description = _('An App Launcher for elementary')
    application_version ="1.1"
    app_years = "2017-2018"
    # main_url = "https://git.mirko.pm/brombinmirko/ElementaryPython"
    # bug_url = "https://git.mirko.pm/brombinmirko/ElementaryPython/issues/labels/bug"
    # help_url = "https://git.mirko.pm/brombinmirko/ElementaryPython/issues"
    # translate_url = "https://git.mirko.pm/brombinmirko/ElementaryPython/blob/master/CONTRIBUTING.md"
    # about_comments = application_description
    about_license_type = Gtk.License.GPL_3_0

class Colors:
    primary_color = "rgba(100, 85, 82, 1)"
    primary_text_color = "#EEEDEC"
    primary_text_shadow_color = "#53433F"

class Syspaths:
    apps_folder = os.getenv("HOME") + "/.local/share/applications/launchy/"
    launchy_folder = os.getenv("HOME") + "/.local/share/launchy/"
    flatpak_prefix = os.getenv("HOME") + "/.var/app/"
    gsettings_schema = "test-schema"

class Templates:
    desktopapp = """
    [Desktop Entry]
    Name=Launchy Template
    GenericName=Web App
    Comment=Launchy Web App
    Exec=notify-send
    Keywords=launchy;webapp;internet
    Icon=application-default-icon
    Type=Application
    Categories=Network;
    Terminal=false;
    StartupWMClass=Launchy
    NoDisplay=true
    """
    userchromeroot = """:root{
    --uc-main-color: %s;
}
"""
