#!/usr/bin/env python3

# TODO:
#   - Agregar un boton de retorno
#   - Cambiar el tipo de transición del widget deck a "Slide"
#   - Crear las funciones para obtener el input de las entradas
#   - Usar FileChooserNative en vez del widget FileChooserDialog
#     - El FileChooserNative no agarra las imágenes
#   - Resetar el ícono cuando hay una entrada en blanco
#     - Pero primero, debo de asignar un icono.
#   - Resetear los campos de entrada asincronamente

import sys
import os
import glob
import gi

import shutil
import warnings
import logging
import desktopfile
import itertools

import webapps as wapps
import constants as cn
import browseractions as browse

gi.require_version('Gtk', '3.0')
gi.require_version('Granite', '1.0')
gi.require_version('Handy', '1')

from gi.repository import Gtk, Gio, Gdk, Handy, Granite, GdkPixbuf
from gi.repository.GLib import GError, Regex, RegexError

class EditView(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(
            self,
            orientation = Gtk.Orientation(1),
            halign = Gtk.Align.FILL,
            homogeneous = True
        )

        self.main_color = None
        self.set_color_scheme()

        try:
            self.protocol_regex = Regex.new("https?:\/\/[\w\d]?", 0, 0)
        except RegexError as elerror:
            logging.critical(f'{elerror}')

        self.gio_settings = Gio.Settings(schema_id=cn.App.application_id)

        message = Gtk.Label.new("Add a Web App")
        message.get_style_context().add_class(Granite.STYLE_CLASS_H2_LABEL)

        self.app_name_entry = Gtk.Entry.new()
        self.app_name_entry.set_placeholder_text("Name")

        self.app_url_entry = Gtk.Entry.new()
        self.app_url_entry.set_placeholder_text("URL")

        self.app_category_entry = Gtk.Entry.new()
        self.app_category_entry.set_placeholder_text("Category")

        self.icon_button = Gtk.Button.new_from_icon_name(
            "application-default-icon",
            Gtk.IconSize.DIALOG
        )

        self.primary_color_button = Gtk.ColorButton.new_with_rgba(self.main_color)

        self.primary_color_button.connect(
            'color-set',
            self.on_color_chosen,
            self.primary_color_button
        )

        self.icon_selector_popover = Gtk.Popover.new(self.icon_button)
        self.icon_selector_popover.set_modal(True)
        self.icon_selector_popover.set_position(Gtk.PositionType.BOTTOM)

        self.icon_button.connect(
            'clicked',
            self.on_clicked_icon_button
        )

        popover_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, spacing = 5)

        self.app_icon_valid = True
        self.app_url_valid = False

        self.icon_name_entry = Gtk.Entry.new()
        self.icon_name_entry.set_placeholder_text("theme icon name")

        or_label = Gtk.Label.new("or")

        icon_chooser_button = Gtk.Button.new_with_label("Set from file...")
        icon_chooser_button.get_style_context().add_class("suggested-action")

        popover_box.set_margin_top(10)
        popover_box.set_margin_bottom(10)
        popover_box.set_margin_left(10)
        popover_box.set_margin_right(10)

        popover_box.pack_start(
            child=self.icon_name_entry,
            expand=True,
            fill=False,
            padding=0
        )

        popover_box.pack_start(
            child=or_label,
            expand=True,
            fill=False,
            padding=0
        )

        popover_box.pack_end(
            child=icon_chooser_button,
            expand=True,
            fill=False,
            padding=0
        )

        popover_box.show_all()

        icon_chooser_button.grab_focus()
        self.icon_selector_popover.add(popover_box)

        app_input_box = Gtk.Box.new(Gtk.Orientation.VERTICAL, spacing = 5)
        app_input_box.set_homogeneous(True)

        self.accept_button = Gtk.Button.new_with_label("Save App")
        self.accept_button.set_halign(Gtk.Align.CENTER)
        self.accept_button.get_style_context().add_class("suggested-action")
        self.accept_button.set_sensitive(False)

        app_input_box.pack_start(
            child=self.app_name_entry,
            expand=False,
            fill=False,
            padding=0
        )

        app_input_box.pack_start(
            child=self.app_url_entry,
            expand=False,
            fill=False,
            padding=0
        )

        app_input_box.pack_start(
            child=self.app_category_entry,
            expand=False,
            fill=False,
            padding=0
        )

        app_info_box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
        app_info_box.set_halign(Gtk.Align.CENTER)
        app_info_box.pack_start(self.icon_button, False, False, 3)
        app_info_box.pack_start(
            child=app_input_box,
            expand=False,
            fill=False,
            padding=3
        )
        app_info_box.pack_start(self.primary_color_button, False, False, 3)

        self.pack_start(
            child=message,
            expand=False,
            fill=False,
            padding=15
        )

        self.pack_start(
            child=app_info_box,
            expand=False,
            fill=False,
            padding=0
        )

        self.pack_end(
            child=self.accept_button,
            expand=False,
            fill=False,
            padding=0
        )

        icon_chooser_button.connect(
            'clicked',
            self.on_icon_chooser_activate
        )

        self.icon_name_entry.connect(
            'changed',
            self.update_app_icon
        )

        self.app_url_entry.connect(
            'changed',
            self.on_app_url_entry_change
        )

        self.accept_button.connect(
            'clicked',
            self.on_accept
        )

    def on_clicked_icon_button(self, widget):
        self.icon_selector_popover.popup()
        print(self.main_color.to_string())

    def set_color_scheme(self):
        self.granite_settings =  Granite.Settings()
        clr_scheme = self.granite_settings.props.prefers_color_scheme
        if clr_scheme == Granite.SettingsColorScheme.DARK:
            self.main_color = Gdk.RGBA(red=0.16, green=0.16, blue=0.16)
        else:
            self.main_color = Gdk.RGBA(red=0.8745, green=0.8745, blue=0.8745)
        
    def get_last_grid_of_category(self, category):
        adj_deck = self.get_parent().get_adjacent_child(0)
        stack = adj_deck.get_children()[2]
        carousel = stack.get_child_by_name(category)
        grid = carousel.get_children()[-1]
        return grid

    def get_carousel_page(self, carousel, page):
        return carousel.get_children()[page]

    def get_effective_grid_items(self, grid):
        lgrid = [item for item in grid if isinstance(item, Gtk.Button)]
        return lgrid

    def get_first_matching_item(self, itemclass, iterable):
        return next(it for it in iterable if isinstance(it, itemclass))

    def get_grid_child_item_location(self, grid, item, prop):
        return grid.child_get_property(item, prop, None)

    def on_new_app_button_pressed2(self, widget):
        self.get_parent().set_can_swipe_back(can_swipe_back=True)
        self.get_parent().navigate(1)

    def add_new_category(self, category, app_path):
        adj_deck = self.get_parent().get_adjacent_child(0)
        stack = adj_deck.get_children()[2]
        appinfo = Gio.DesktopAppInfo.new_from_filename(app_path)
        applist = [-1, appinfo, None, None, None, None]
        grid = wapps.WebAppView.create_grid(self)
        carousel = Handy.Carousel.new()
        row = 0
        col = 0
        for app in applist:
            btn = wapps.WebAppView.get_button(wapps.WebAppView, app)
            grid.attach(btn, col, row, 1, 1)
            col += 1
            if col == 3:
                col = 0
                row += 1

        carousel.insert(grid, -1)
        name = "%s" % category
        title = "%s" % category
        stack.add_titled(carousel, name, title)
        grid.show()
        grid.show_all()

    def on_icon_chooser_activate(self, widget):
        filter = Gtk.FileFilter.new()
        filter.set_name("Images")
        filter.add_mime_type("image/*")

        file_chooser = Gtk.FileChooserDialog(
            "Choose Icon",
            None,
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        )

        file_chooser.set_select_multiple(False)
        file_chooser.add_filter(filter)

        response = file_chooser.run()

        if response == Gtk.ResponseType.OK:
            self.icon_name_entry.set_text(file_chooser.get_filename())
            file_chooser.destroy()

        file_chooser.destroy()

    def reset_fields(self):
        self.app_name_entry.set_text("")
        self.icon_name_entry.set_text("")
        self.app_url_entry.set_text("")
        self.app_category_entry.set_text("")
        # self.app_url_entry.sensitive = true
        self.app_name_entry.get_style_context().remove_class("error")
        self.app_url_entry.get_style_context().remove_class("error")
        self.icon_button.set_image(
            Gtk.Image.new_from_icon_name(
                "application-default-icon",
                Gtk.IconSize.DIALOG
            )
        )

    def validate (self):
        if (self.app_icon_valid and self.app_url_valid):
            self.accept_button.set_sensitive(True)
        else:
            self.accept_button.set_sensitive(False)

    def on_color_chosen(self, data, button):
        self.main_color = button.get_rgba()
        print(button.get_rgba())

    def on_app_url_entry_change(self, widget):
        if not self.protocol_regex.match(self.app_url_entry.get_text(), 0)[0]:
            self.app_url_entry.get_style_context().add_class('error')
            self.app_url_entry.set_icon_from_icon_name(
                Gtk.EntryIconPosition(1),
                "dialog-information"
            )
            self.app_url_entry.set_icon_tooltip_text(
                Gtk.EntryIconPosition(1),
                "url must start with http(s)://"
            )
            self.app_url_valid = False
        else:
            self.app_url_entry.set_icon_from_icon_name(
                Gtk.EntryIconPosition(1),
                None
            )
            self.app_url_entry.get_style_context().remove_class("error")
            self.app_url_valid = True
        self.validate()


    def update_app_icon (self, widget):
        icon = self.icon_name_entry.get_text()

        if icon == "":
            self.app_icon_valid = True
            self.validate()
            return

        if icon.__contains__("/"):
            pix = None
            try:
                self.app_icon_valid = True
                pix = GdkPixbuf.Pixbuf.new_from_file_at_size(icon, 48, 48)
            except GError:
                self.app_icon_valid = False
                try:
                    icon_theme = Gtk.IconTheme.get_default()
                    pix = icon_theme.load_icon(
                        "image-missing", 48,
                        Gtk.IconLookupFlags.FORCE_SIZE)
                except GError:
                    warnings.warn("Getting selection-checked icon from theme failed")
            if pix is not None:
                self.icon_button.set_image(Gtk.Image.new_from_pixbuf(pix))

        else:
            self.icon_button.set_image(
                Gtk.Image.new_from_icon_name(icon, Gtk.IconSize.DIALOG)
            )

        self.validate()

    def lastest_file(self):
        f = cn.Syspaths.apps_folder + "*.desktop"
        newest = max(glob.iglob(f), key = os.path.getctime, default=2)
        return newest

    def add_new_button(self, app, category):
        appinfo = Gio.DesktopAppInfo.new_from_filename(app)
        grid = self.get_last_grid_of_category(category)
        adj_deck = self.get_parent().get_adjacent_child(0)
        stack = adj_deck.get_children()[2]
        carousel = stack.get_child_by_name(category)

        if len(self.get_effective_grid_items(grid.get_children())) == 6:
            applist = (appinfo, None, None, None, None, None)
            newgrid = wapps.WebAppView.create_grid(self)
            row = 0
            col = 0
            for app in applist:
                btn =  wapps.WebAppView.get_button(wapps.WebAppView, app)
                newgrid.attach(btn, col, row, 1, 1)
                col += 1
                if col == 3:
                    col = 0
                    row += 1

            newgrid.show()
            newgrid.show_all()

            carousel.insert(newgrid, -1)

        else:
            wapps.WebAppView.add_a_button_to_grid(grid, appinfo)
            grid.show()
            grid.show_all()

    def on_accept(self, widget):
        app_icon = self.icon_name_entry.get_text()
        app_category = self.app_category_entry.get_text()
        app_name = self.app_name_entry.get_text()
        app_url = self.app_url_entry.get_text()

        schema = self.gio_settings.get_strv("app-categories")

        desktopfile.LaunchyDesktopFile.create(
            app_name,
            app_url,
            app_icon,
            app_category
        )

        newest = self.lastest_file()

        if not app_category in schema:
            schema.append(app_category)
            self.gio_settings.set_strv("app-categories", schema)
            self.add_new_category(app_category, newest)
        else:
            self.add_new_button(newest, app_category)

        profilename = app_name.replace(" ", "")

        rootstr = cn.Templates.userchromeroot % self.main_color.to_string()

        cmd = browse.BrowserActions.create_profile_argument(profilename)
        os.system(cmd)

        last_profile_dir = browse.BrowserActions.get_profile_folder(
            profilename,
            cn.Syspaths.flatpak_prefix + "io.gitlab.librewolf-community/.librewolf")

        src_userjs = cn.Syspaths.launchy_folder + "user.js"
        src_userchrome = cn.Syspaths.launchy_folder + "userChrome.css"

        chromepath  = os.path.join(last_profile_dir, 'chrome')
        os.makedirs(chromepath)

        shutil.copy(src_userjs, last_profile_dir)

        outpath = chromepath + '/userChrome.css'

        with open(src_userchrome, 'r') as src, open(outpath, 'x') as o:
            css = src.read()
            o.write(rootstr + css)

        self.reset_fields()

        self.get_parent().navigate(0)
