#!/usr/bin/env python3

import gi
import sys
import os
import constants

from gi.repository import Gio, Gdk, Gtk

# TODO:
#   - Función de filtro
#   - Función que obtiene los diversos parametros del desktopfile
#   - Try/Exception cuando hay un error en los desktopfiles

def desktopfilelist():
    """Regresa una lista de Gio.DesktopAppInfo's a partir de los desktopfiles en el directorio"""
    p = constants.Syspaths.apps_folder
    desktopvar = [Gio.DesktopAppInfo.new("launchy-" + f) for f in os.listdir(p)]
    return desktopvar

def filter_apps(category):
    unflt = desktopfilelist()
    flt = [app for app in unflt if (app.get_categories()[:-1] == category)]
    return flt
