#!/usr/bin/env python3

import gi
import os
import locale
import gettext

gi.require_version('Gtk', '3.0')
gi.require_version('Granite', '1.0')

from gi.repository import Gtk, Granite

class WelcomeView(Gtk.Box):

    settings = Gtk.Settings.get_default()

    def __init__(self):
        Gtk.Box.__init__(self, False, 0)

        # '''Your app needs translations, right?
        # Here we are trying to set the locale_path to the system one, assuming
        # the app is installed.'''
        # try:
        #     current_locale, encoding = locale.getdefaultlocale()
        #     locale_path = os.path.join(
        #         os.path.abspath(
        #             os.path.dirname(__file__)
        #         ),
        #         'locale'
        #     )
        #     translate = gettext.translation(
        #         cn.App.application_shortname,
        #         locale_path,
        #         [current_locale]
        #     )
        #     _ = translate.gettext
        # except FileNotFoundError:
        #     _ = str
        # ''''The self._ can be used for defining a new translation string.
        # Note: that if the translation is not loaded (the check above),
        #       self._ will be the same as str, so you won't get any errors.'''
        # self._ = _

        welcome = Granite.WidgetsWelcome().new(
            "No websites found",
            "Add your first app."
        );

        welcome.append(
            "list-add",
            'Launch a website!',
            'Create a new web app'
        );

        welcome.connect("activated", self.on_welcome_activated)

        self.add(welcome)

    def on_welcome_activated(self, widget, index):
        self.get_parent().navigate(1)
        self.get_parent().navigate(1)
        self.get_parent().destroy
