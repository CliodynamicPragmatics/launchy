#!/usr/bin/env python3
import gi
import sys

gi.require_version('Gtk', '3.0')
gi.require_version('Granite', '1.0')
gi.require_version('Handy', '1')

from gi.repository import Gtk, Gio, Gdk, Handy, Granite

class LaunchyAppButton():
    def new_button(appinfo):
        button = Gtk.Button.new_with_label(appinfo.get_name())
        button.set_relief(Gtk.ReliefStyle(2))
        image = Gtk.Image.new_from_gicon(
            appinfo.get_icon(),
            Gtk.IconSize(6)
        )
        button.set_image(image)
        button.set_image_position(Gtk.PositionType(2))
        button.set_always_show_image(True)
        return button

    def attach_to_gird(egrid, item, x, y):
        if isinstance(item, None):
            egrid.attach(item, x, y, 1, 1)
        else:
            egrid.get_child_at(x, y).destroy()
            egrid.attach.item(item, x, y, 1, 1)
