#!/usr/bin/env python3

import os
import gi
import sys
import constants as cn
import desktopfilemanager as dfm
import launchyappbutton as lab

from itertools import zip_longest

gi.require_version('Gtk', '3.0')
gi.require_version('Granite', '1.0')
gi.require_version('Handy', '1')

from gi.repository import Gtk, Gio, Gdk, Handy, Granite

class WebAppView(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(
            self, orientation = Gtk.Orientation(0),
            halign = Gtk.Align.FILL,
        )

        self.on_get_webapps_view

        self.gschema = Gio.Settings(schema_id=cn.App.application_id)

        self.separator = Gtk.Separator.new(Gtk.Orientation(1))

        self.stack = Gtk.Stack.new()
        self.stack.set_transition_type(
            transition=Gtk.StackTransitionType(0)
        )

        self.stack.set_transition_duration(duration=1500)

        for category in self.gschema.get_strv("app-categories"):
            carousel = Handy.Carousel.new()
            app_list = dfm.filter_apps(category)

            app_list.insert(0, -1)
            split = self.split_applist(6, app_list, None)

            for sublist in split:
                grid = self.create_grid()
                self.add_buttons_to_grid(grid, sublist)
                carousel.insert(grid, -1)

                name = "%s" % category
                title = "%s" % category
                self.stack.add_titled(carousel, name, title)

        self.stackbar=Gtk.StackSidebar.new()
        self.stackbar.set_stack(self.stack)

        self.pack_start(
            child=self.stackbar,
            expand=False,
            fill=False,
            padding=0
        )

        self.pack_start(
            child=self.separator,
            expand=False,
            fill=False,
            padding=0
        )

        self.pack_end(
            child=self.stack,
            expand=True,
            fill=True,
            padding=0
        )

    def split_applist(self, n, iterable, padvalue=None):
        return zip_longest(*[iter(iterable)]*n, fillvalue=padvalue)

    def create_grid(self):
        grid = Gtk.Grid.new()
        grid.set_hexpand(True)
        grid.set_vexpand(False)
        grid.set_row_homogeneous(True)
        grid.set_column_homogeneous(True)
        grid.set_margin_top(12)
        grid.set_margin_start(12)
        grid.set_margin_end(12)
        grid.set_margin_bottom(12)
        grid.set_row_spacing(12)
        grid.set_column_spacing(0)

        return grid

    def launch_app(self, app):
        os.system(app.get_commandline())
    
    def get_button(self, app):
        if isinstance(app, int):
            button = Gtk.Button.new_from_icon_name(
                "list-add-symbolic",
                Gtk.IconSize(6)
            )

            button.connect(
                'clicked',
                self.on_new_app_button_pressed
            )

        elif app is None:
            button = Gtk.Label(" ")
        else:
            button = lab.LaunchyAppButton.new_button(app)
            button.connect(
                'clicked',
                self.on_button_clicked,
                app
            )
        return button

    def add_buttons_to_grid(self, grid, applist):
        row = 0
        col = 0
        for app in applist:
            button = self.get_button(app)
            print(type(app))
            grid.attach(button, col, row, 1, 1)
            col += 1
            if col == 3:
                col = 0
                row += 1
        return grid


    def add_a_button_to_grid(grid, newapp):
        grid_items = grid.get_children()
        grid_items.reverse()

        first_label = next(i for i in grid_items if isinstance(i, Gtk.Label))
        col = grid.child_get_property(first_label, "left-attach", None)
        row = grid.child_get_property(first_label, "top-attach", None)

        grid.get_child_at(col, row).destroy()

        new_button = lab.LaunchyAppButton.new_button(newapp)
        grid.attach(new_button, col, row, 1, 1)

        grid.show()
        grid.show_all()

    def on_new_app_button_pressed(self, widget):
        self.get_parent().set_can_swipe_back(can_swipe_back=True)
        self.get_parent().navigate(1)

    def on_get_webapps_view(self):
        self.get_parent().set_can_swipe_back(can_swipe_back=False)

    def on_button_clicked(self, widget, app):
        app.launch()
        self.get_toplevel().close()
